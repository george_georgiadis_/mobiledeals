var path = require('path');

var common = require('../webpack/config');

var config = {
  resolve: common.resolve,
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: 'style-loader!css!postcss-loader!sass-loader'
      }
    ]
  }
}

module.exports = config;
